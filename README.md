# kotlin

The Kotlin Programming Language http://kotlinlang.org/

# Similar projects
* https://gitlab.com/hub-docker-com-demo/kotlin
* https://gitlab.com/javascript-packages-demo/kotlinc-js-api

## Bug
* Requires a systemd enabled image, which seems not to be available right now!